package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	file, err := os.Open("./day1.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var lineVal []int
	lineTrack := [2]string{"a", "a"}
	subStr := ""
	checkerVal := ""
	lineNum := ""
	intNum := 0

	// optionally, resize scanner's capacity for lines over 64K, see next example
	for scanner.Scan() {
		for i, c := range scanner.Text() {
			if unicode.IsNumber(c) {
				if lineTrack[0] == "a" {
					lineTrack[0] = string(c)
					lineTrack[1] = string(c)
				} else {
					lineTrack[1] = string(c)
				}
			} else {
				if i+5 <= len(scanner.Text()) {
					subStr = scanner.Text()[i : i+5]
				} else {
					subStr = scanner.Text()[i:]
				}
				checkerVal = checker(subStr)

				if checkerVal != "0" {
					// fmt.Println(checkerVal)
					if lineTrack[0] == "a" {
						lineTrack[0] = string(checkerVal)
						lineTrack[1] = string(checkerVal)
					} else {
						lineTrack[1] = string(checkerVal)
					}
				}
			}
			// fmt.Println(lineTrack)

		}
		lineNum = lineTrack[0] + lineTrack[1]
		intNum, _ = strconv.Atoi(lineNum)
		lineVal = append(lineVal, intNum)
		lineTrack = [2]string{"a", "a"}
		// fmt.Println(lineVal)
	}
	fmt.Println(sumArray(lineVal))
}

func checker(str string) string {
	repStr := str
	numWords := [9]string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}
	numNums := [9]string{"1", "2", "3", "4", "5", "6", "7", "8", "9"}
	for i, c := range numWords {
		repStr = strings.Replace(repStr, c, numNums[i], -1)
		// fmt.Println(repStr)
	}
	// fmt.Println(repStr)
	for _, c := range repStr {
		if unicode.IsNumber(rune(c)) {
			return string(c)
		}
	}
	return "0"
}

func sumArray(arr []int) int {
	sum := 0
	for _, valueInt := range arr {
		sum += valueInt
	}
	return sum
}
