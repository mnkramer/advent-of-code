package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	// "unicode"
)

func main() {
	file, err := os.Open("./day2.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var game []string
	var allDraws []string
	var gameNum int
	var sumIDs int
	var sumPowers int

	for scanner.Scan() {
		game = strings.Split(scanner.Text(), ": ")
		gameNum, _ = strconv.Atoi(strings.Split(game[0], " ")[1])
		allDraws = strings.Split(game[1], "; ")
		// fmt.Println(gameNum)
		// fmt.Println(allDraws)
		if testPossible(allDraws) {
			sumIDs += gameNum
		}
		sumPowers += findMinCubes(allDraws)
	}

	fmt.Println("Sum of Possible Games (pt1): ", sumIDs)
	fmt.Println("Sum of Powers of Min Cubes (pt2): ", sumPowers)
}

func testPossible(allDraws []string) bool {
	var singleDraw []string
	var color string
	var num int
	maxDraw := map[string]int{
		"red":   12,
		"green": 13,
		"blue":  14,
	}

	for _, c := range allDraws {
		singleDraw = strings.Split(c, ", ")
		// fmt.Println(singleDraw)
		for _, draw := range singleDraw {
			num, _ = strconv.Atoi(strings.Split(draw, " ")[0])
			color = strings.Split(draw, " ")[1]
			if num > maxDraw[color] {
				return false
			}
		}
	}

	return true
}

func findMinCubes(allDraws []string) int {
	var singleDraw []string
	var color string
	var num int
	var power int = 1
	maxDraw := map[string]int{
		"red":   0,
		"green": 0,
		"blue":  0,
	}

	for _, c := range allDraws {
		singleDraw = strings.Split(c, ", ")
		// fmt.Println(singleDraw)
		for _, draw := range singleDraw {
			num, _ = strconv.Atoi(strings.Split(draw, " ")[0])
			color = strings.Split(draw, " ")[1]
			if num > maxDraw[color] {
				maxDraw[color] = num
			}
		}
	}
	for _, value := range maxDraw {
		// fmt.Println(power, value)
		power = power * value
	}
	// fmt.Println(power)
	return power
}
