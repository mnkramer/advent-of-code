package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"unicode"
)

func main() {
	fullArray := setupArray("./day3_part1.txt")
	trackingNum := ""
	// startInd := 0
	check := false
	num := 0
	numSize := 0
	sumP1 := 0
	sumP2 := 0

	// fmt.Println(fullArray)

	for yInd, y := range fullArray {
		for xInd, val := range y {
			// Code for pt1
			if unicode.IsDigit(val) && val != 'H' {
				trackingNum = trackingNum + string(val)
				numSize += 1
				// if trackingNum == "" {
				// 	startInd = yInd
				// }
			} else {
				if trackingNum != "" {
					// fmt.Println(yInd, xInd, numSize, trackingNum)
					check, num = checkNeighbors(yInd, xInd-numSize, trackingNum, numSize, fullArray)
					if check {
						sumP1 += num
					}
					num = 0
					numSize = 0
					trackingNum = ""
				}

			}
			if val == '*' {
				check, num = checkGears(yInd, xInd, fullArray)
				if check {
					sumP2 += num
				}
			}
		}
	}
	fmt.Println("The sum is (pt1): ", sumP1)
	fmt.Println("The sum is (pt2): ", sumP2)
}

func checkGears(yInd, xInd int, arr [][]rune) (bool, int) {
	var minPos int
	var length int
	var num int
	var numStr string
	var numTrack []int

	for y := (yInd - 1); y <= (yInd + 1); y++ {
		for x := (xInd - 1); x <= (xInd + 1); x++ {
			if unicode.IsNumber(arr[y][x]) {
				length += 1
				minPos = x

				if unicode.IsNumber(arr[y][x+1]) {
					length += 1
					if unicode.IsNumber(arr[y][x+2]) {
						length += 1
					}
				}

				if unicode.IsNumber(arr[y][x-1]) {
					length += 1
					minPos = x - 1
					if unicode.IsNumber(arr[y][x-2]) {
						length += 1
						minPos = x - 2
					}
				}

				for i := 0; i < length; i++ {
					numStr += string(arr[y][minPos+i])
				}

				num, _ = strconv.Atoi(numStr)
				if !slices.Contains(numTrack, num) {
					numTrack = append(numTrack, num)
				}

				numStr = ""
				length = 0
			}
			// fmt.Println(numStr, numTrack)
		}
	}
	if len(numTrack) == 2 {
		return true, multiplyArray(numTrack)
	} else {
		return false, 0
	}
}

func checkNeighbors(yInd, xInd int, num string, size int, arr [][]rune) (bool, int) {
	symbols := []rune{'*', '&', '$', '/', '%', '=', '+', '@', '-', '#'}
	var neighbors []rune
	intNum, _ := strconv.Atoi(num)

	// fmt.Println(symbols)
	for y := (yInd - 1); y <= (yInd + 1); y++ {
		for x := (xInd - 1); x <= (xInd + size); x++ {
			neighbors = append(neighbors, arr[y][x])
		}
	}
	// fmt.Println(neighbors)
	for _, n := range neighbors {
		if runeInSlice(n, symbols) {
			// fmt.Println(intNum)
			return true, intNum
		}
	}
	return false, intNum
}

func setupArray(filepath string) [][]rune {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var singleX []rune
	var fullArray [][]rune

	for i := 0; i < 143; i++ {
		singleX = append(singleX, 'H')
	}
	// fmt.Println(singleX)

	fullArray = append(fullArray, singleX)
	singleX = []rune{}

	// fmt.Println(singleX)
	// fmt.Println(fullArray)

	for scanner.Scan() {
		singleX = append(singleX, 'H')
		for _, c := range scanner.Text() {
			singleX = append(singleX, rune(c))
		}
		singleX = append(singleX, 'H')
		fullArray = append(fullArray, singleX)
		singleX = []rune{}

	}

	for i := 0; i < 143; i++ {
		singleX = append(singleX, 'H')
	}
	fullArray = append(fullArray, singleX)

	return fullArray
}

func runeInSlice(a rune, list []rune) bool {
	for _, b := range list {
		// fmt.Println("HELP")
		if b == a {
			// fmt.Println("HELP")
			return true
		}
	}
	return false
}

func multiplyArray(arr []int) int {
	prod := 1
	for _, valueInt := range arr {
		prod = prod * valueInt
	}
	return prod
}
