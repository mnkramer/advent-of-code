package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	allCards := setupArray("./day4.txt")

	var card []string
	var points int
	var numbers []string
	var winNums []string
	var numsHave []string
	var cardWinnings []int

	for _, c := range allCards {
		card = strings.Split(c, ": ")
		numbers = strings.Split(card[1], " | ")
		winNums = strings.Split(numbers[0], " ")
		numsHave = strings.Split(numbers[1], " ")
		// fmt.Println(card[0], winNums, numsHave)
		for _, num := range numsHave {
			if winCheck(num, winNums) {
				if points < 1 {
					points = 1
				} else {
					points = points * 2
				}
			}
		}

		cardWinnings = append(cardWinnings, points)
		points = 0
	}

	wins := sumArray(cardWinnings)

	fmt.Println("Total scratch winnings (pt1): ", wins)

	finalCards := totalCards(allCards)

	fmt.Println("Total scratch cards (pt2): ", finalCards)

}

func totalCards(allCards []string) int {
	var card []string
	var numbers []string
	var winNums []string
	var numsHave []string
	var finalCards int
	cardCount := make(map[int]int, (len(allCards) - 1))
	var nextCards int
	var keys []int

	for ind := range allCards {
		cardCount[ind] = 1
		keys = append(keys, ind)
	}

	// fmt.Println(keys)
	for _, cardNum := range keys {
		// fmt.Println(cardNum, cardCount[cardNum])
		card = strings.Split(allCards[cardNum], ": ")
		numbers = strings.Split(card[1], " | ")
		winNums = strings.Split(numbers[0], " ")
		numsHave = strings.Split(numbers[1], " ")
		// fmt.Println(card[0], winNums, numsHave)
		for count := 0; count < cardCount[cardNum]; count++ {
			for _, num := range numsHave {
				if winCheck(num, winNums) {
					// print(nextCards)
					nextCards += 1
					if (cardNum + nextCards) <= (len(allCards) - 1) {
						cardCount[cardNum+nextCards] += 1
					}

				}
			}
			nextCards = 0
			finalCards += 1
		}
	}
	return finalCards
}

func setupArray(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var finalArray []string

	for scanner.Scan() {
		finalArray = append(finalArray, scanner.Text())
	}

	return finalArray
}

func winCheck(str string, poss []string) bool {
	for _, num := range poss {
		if str == num && str != "" {
			return true
		}
	}

	return false
}

func sumArray(arr []int) int {
	sum := 0
	for _, num := range arr {
		sum += num
	}

	return sum
}
