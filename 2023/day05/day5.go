package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	toLookup, lookupTable := makeMaps("./day5.txt")
	part1Lookup := toLookup
	// var part2Lookup []int
	valToLookup := 0
	pt1Lowest := -1
	// pt2Lowest := -1
	var isSeed bool
	locStore := 0

	for _, val := range part1Lookup {
		valToLookup = val
		for _, lookup := range lookupTable {
			// fmt.Println(toLookup, lookup)
			valToLookup = singleValLookup(valToLookup, lookup)
		}
		if valToLookup < pt1Lowest || pt1Lowest == -1 {
			pt1Lowest = valToLookup
		}
	}

	fmt.Println("The lowest location (pt1): ", pt1Lowest)

	// for i := 0; i < len(toLookup); i += 2 {
	// 	for j := 0; j < toLookup[i+1]; j++ {
	// 		valToLookup = (toLookup[i] + j)
	// 		// part2Lookup = append(part2Lookup, (toLookup[i] + j))
	// 		for _, lookup := range lookupTable {
	// 			// fmt.Println(toLookup, lookup)
	// 			valToLookup = singleValLookup(valToLookup, lookup)
	// 		}
	// 		if valToLookup < pt2Lowest || pt2Lowest == -1 {
	// 			pt2Lowest = valToLookup
	// 		}
	// 	}
	// }
	// fmt.Println("The lowest location (pt2): ", pt2Lowest)

	for i := 0; i > -1; i++ {
		for j := 6; j >= 0; j-- {
			if j == 6 {
				locStore = i
				valToLookup = lookupBackwards(i, lookupTable[j])
			} else {
				valToLookup = lookupBackwards(valToLookup, lookupTable[j])
			}
		}
		for s := 0; s < len(toLookup); s += 2 {
			if valToLookup >= toLookup[s] && valToLookup <= toLookup[s]+toLookup[s+1] {
				isSeed = true
			}
		}
		if isSeed {
			break
		}
	}
	fmt.Println("The lowest location (pt2): ", locStore)

}

func makeMaps(filename string) ([]int, [][]string) {
	var seedText []string
	var seedNums []int
	var currentStr string
	var seedNum int
	var currentLookup []string
	var allLookups [][]string

	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		if len(seedNums) == 0 {
			seedText = strings.Split(strings.Split(scanner.Text(), ": ")[1], " ")
			for _, seed := range seedText {
				seedNum, _ = strconv.Atoi(seed)
				seedNums = append(seedNums, seedNum)
			}
		} else {
			if scanner.Text() != "" {
				currentStr = scanner.Text()
				// fmt.Println(currentStr)
				if unicode.IsDigit(rune(currentStr[0])) {

					currentLookup = append(currentLookup, currentStr)
					// fmt.Println(currentLookup)

				} else {
					if len(currentLookup) != 0 {
						allLookups = append(allLookups, currentLookup)
					}
					// fmt.Println(currentLookup) //, allLookups)
					currentLookup = []string{}
					// fmt.Println(currentLookup, allLookups)
				}
			}
		}
	}
	if len(currentLookup) != 0 {
		allLookups = append(allLookups, currentLookup)
	}
	// fmt.Println(currentLookup) //, allLookups)
	currentLookup = []string{}
	// fmt.Println(seedNums, allLookups)
	return seedNums, allLookups
}

func valLookup(vals []int, lookup []string) []int {
	// NO LONGER USED KEEPING THIS FOR MEMORIES
	var endVals, base []int
	var destStart, sourceStart, count, track int
	var mapNums []string

	for _, val := range vals {
		for _, info := range lookup {
			mapNums = strings.Split(info, " ")
			destStart, _ = strconv.Atoi(mapNums[0])
			sourceStart, _ = strconv.Atoi(mapNums[1])
			count, _ = strconv.Atoi(mapNums[2])
			if val >= sourceStart && val < (sourceStart+count) {
				track = destStart + (val - sourceStart) //destStart + (count - val)
				endVals = append(endVals, track)
				base = append(base, val)
				// fmt.Println(val, track, sourceStart, (sourceStart + count))
			}
			// for i := 0; i < count; i++ {
			// 	if (i + sourceStart) == val {
			// 		// fmt.Println((i + sourceStart), val, (i + destStart))
			// 		endVals = append(endVals, (destStart + i))
			// 		base = append(base, val)
			// 	}
			// }
		}
		if !contains(base, val) {
			endVals = append(endVals, val)
		}

	}
	// fmt.Println(endVals)
	return endVals
}

func contains(arr []int, val int) bool {
	for _, i := range arr {
		if val == i {
			return true
		}
	}
	return false
}

func singleValLookup(val int, lookup []string) int {
	var destStart, sourceStart, count, track int
	var mapNums []string

	for _, info := range lookup {
		mapNums = strings.Split(info, " ")
		destStart, _ = strconv.Atoi(mapNums[0])
		sourceStart, _ = strconv.Atoi(mapNums[1])
		count, _ = strconv.Atoi(mapNums[2])
		if val >= sourceStart && val < (sourceStart+count) {
			track = destStart + (val - sourceStart) //destStart + (count - val)
			return track
			// fmt.Println(val, track, sourceStart, (sourceStart + count))
		}
		// for i := 0; i < count; i++ {
		// 	if (i + sourceStart) == val {
		// 		// fmt.Println((i + sourceStart), val, (i + destStart))
		// 		endVals = append(endVals, (destStart + i))
		// 		base = append(base, val)
		// 	}
		// }
	}
	return val
}

func lookupBackwards(val int, lookup []string) int {
	var destStart, sourceStart, count, track int
	var mapNums []string

	for _, info := range lookup {
		mapNums = strings.Split(info, " ")
		destStart, _ = strconv.Atoi(mapNums[0])
		sourceStart, _ = strconv.Atoi(mapNums[1])
		count, _ = strconv.Atoi(mapNums[2])
		if val >= destStart && val < (destStart+count) {
			track = sourceStart + (val - destStart) //destStart + (count - val)
			return track
			// fmt.Println(val, track, sourceStart, (sourceStart + count))
		}
		// for i := 0; i < count; i++ {
		// 	if (i + sourceStart) == val {
		// 		// fmt.Println((i + sourceStart), val, (i + destStart))
		// 		endVals = append(endVals, (destStart + i))
		// 		base = append(base, val)
		// 	}
		// }
	}
	return val
}
