package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file := "day6.txt"

	mappings := readInputPt1(file)
	var totalWins []int

	for time, dist := range mappings {
		totalWins = append(totalWins, checkAllSpeeds(time, dist))
	}

	waysToWin := multArray(totalWins)

	fmt.Println("Total ways to win (pt1): ", waysToWin)

	singleRaceNums := readInputPt2(file)

	singleRaceSols := checkAllSpeeds(singleRaceNums[0], singleRaceNums[1])

	fmt.Println("Total ways to win (pt2): ", singleRaceSols)
}

func checkAllSpeeds(time, dist int) int {
	var totalWins, totalDist, speed int

	for i := 0; i <= time; i++ {
		speed = i
		totalDist = (speed * (time - i))
		if totalDist > dist {
			totalWins += 1
		}
	}

	return totalWins
}

func readInputPt1(filename string) map[int]int {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var text, nums []string
	var numCheck int
	var tracker int
	var timeTrack, distTrack []int
	finalMap := make(map[int]int)

	for scanner.Scan() {
		text = strings.Split(scanner.Text(), ": ")
		nums = strings.Split(text[1], " ")

		for _, num := range nums {
			numCheck, _ = strconv.Atoi(num)
			if numCheck != 0 {
				if tracker == 0 {
					timeTrack = append(timeTrack, numCheck)
				} else if tracker == 1 {
					distTrack = append(distTrack, numCheck)
				}
			}
		}
		tracker += 1
	}

	for i := 0; i < len(timeTrack); i++ {
		finalMap[timeTrack[i]] = distTrack[i]
	}
	// fmt.Println(timeTrack, distTrack)
	// fmt.Println(finalMap)

	return finalMap
}

func readInputPt2(filename string) []int {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var numStorage []int
	var text []string
	var numStr string
	var num int

	for scanner.Scan() {
		text = strings.Split(scanner.Text(), ": ")

		numStr = strings.ReplaceAll(text[1], " ", "")
		num, _ = strconv.Atoi(numStr)

		numStorage = append(numStorage, num)
	}

	// fmt.Println(numStorage)

	return numStorage
}

func multArray(arr []int) int {
	prod := 1
	for _, num := range arr {
		prod = prod * num
	}

	return prod
}
