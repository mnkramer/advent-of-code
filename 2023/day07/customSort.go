package main

import "unicode/utf8"

// THIS CUSTOM ALPHABET SORT IS TAKEN FROM STACKOVERFLOW:
// https://stackoverflow.com/questions/53902528/how-can-i-define-a-custom-alphabet-order-for-comparing-and-sorting-strings-in-go

// As I write this on day 7, my 7 days of Go experience along with very limited knowledge of various
// software engineering concepts/ideas, I would not have been able to implement this myself, and I want to
// work through all the advent of code days if I can.

const alphabet = "AKQJT98765432"
const alphabet2 = "AKQT98765432J"

var weights = map[rune]int{}
var weights2 = map[rune]int{}

func init() {
	for i, r := range alphabet {
		weights[r] = i
	}
	for i, r := range alphabet2 {
		weights2[r] = i
	}
}

func less(s1, s2 string) bool {
	for {
		switch e1, e2 := len(s1) == 0, len(s2) == 0; {
		case e1 && e2:
			return false // Both empty, they are equal (not less)
		case !e1 && e2:
			return false // s1 not empty but s2 is: s1 is greater (not less)
		case e1 && !e2:
			return true // s1 empty but s2 is not: s1 is less
		}

		r1, size1 := utf8.DecodeRuneInString(s1)
		r2, size2 := utf8.DecodeRuneInString(s2)

		// Check if both are custom, in which case we use custom order:
		custom := false
		if w1, ok1 := weights[r1]; ok1 {
			if w2, ok2 := weights[r2]; ok2 {
				custom = true
				if w1 != w2 {
					return w1 < w2
				}
			}
		}
		if !custom {
			// Fallback to numeric rune comparison:
			if r1 != r2 {
				return r1 < r2
			}
		}

		s1, s2 = s1[size1:], s2[size2:]
	}
}

func less2(s1, s2 string) bool {
	for {
		switch e1, e2 := len(s1) == 0, len(s2) == 0; {
		case e1 && e2:
			return false // Both empty, they are equal (not less)
		case !e1 && e2:
			return false // s1 not empty but s2 is: s1 is greater (not less)
		case e1 && !e2:
			return true // s1 empty but s2 is not: s1 is less
		}

		r1, size1 := utf8.DecodeRuneInString(s1)
		r2, size2 := utf8.DecodeRuneInString(s2)

		// Check if both are custom, in which case we use custom order:
		custom := false
		if w1, ok1 := weights2[r1]; ok1 {
			if w2, ok2 := weights2[r2]; ok2 {
				custom = true
				if w1 != w2 {
					return w1 < w2
				}
			}
		}
		if !custom {
			// Fallback to numeric rune comparison:
			if r1 != r2 {
				return r1 < r2
			}
		}

		s1, s2 = s1[size1:], s2[size2:]
	}
}
