package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"reflect"
	"sort"
	"strconv"
	"strings"
)

func main() {
	handMap := readHands("./day7.txt")

	var totalHands = map[int][]string{}
	var totalHandsPt2 = map[int][]string{}
	var handType, handTypePt2, currentMult, currentMultPt2, sum, sumPt2, bet, betPt2 int
	var handsOrdered, handsOrderedPt2 []string

	for hand := range handMap {
		handType = getType(hand)
		totalHands[handType] = append(totalHands[handType], hand)
	}

	// fmt.Println(totalHands)

	for i := 0; i < len(totalHands); i++ {
		// fmt.Println(hands)
		totalHands[i] = sortHands(totalHands[i])
		handsOrdered = append(handsOrdered, totalHands[i]...)
	}
	// fmt.Println(handsOrdered)

	for i := 0; i < len(handsOrdered); i++ {
		currentMult = len(handsOrdered) - i
		bet = handMap[handsOrdered[i]]
		sum += (bet * currentMult)
	}
	fmt.Println("The total winnings (pt1): ", sum)

	for hand := range handMap {
		handTypePt2 = getTypePt2(hand)
		totalHandsPt2[handTypePt2] = append(totalHandsPt2[handTypePt2], hand)
	}

	// fmt.Println(totalHands)

	for i := 0; i < len(totalHandsPt2); i++ {
		totalHandsPt2[i] = sortHandsPt2(totalHandsPt2[i])
		handsOrderedPt2 = append(handsOrderedPt2, totalHandsPt2[i]...)
	}
	// fmt.Println(handsOrderedPt2)

	for i := 0; i < len(handsOrderedPt2); i++ {
		currentMultPt2 = len(handsOrderedPt2) - i
		betPt2 = handMap[handsOrderedPt2[i]]
		sumPt2 += (betPt2 * currentMultPt2)
	}

	fmt.Println("The total winnings (pt2): ", sumPt2)
}

func readHands(filename string) map[string]int {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	handMap := make(map[string]int)
	var handCards string
	var handBet int
	var handInfo []string

	for scanner.Scan() {
		handInfo = strings.Split(scanner.Text(), " ")
		handCards = handInfo[0]
		handBet, _ = strconv.Atoi(handInfo[1])
		handMap[handCards] = handBet
		// fmt.Println(handCards, handBet, handMap)
	}

	// fmt.Println(handMap)

	return handMap
}

func getType(hand string) int {
	var typeTrack = map[int]map[int]int{0: {5: 1}, 1: {4: 1, 1: 1}, 2: {3: 1, 2: 1}, 3: {3: 1, 1: 2}, 4: {2: 2, 1: 1}, 5: {2: 1, 1: 3}, 6: {1: 5}}
	handLookup := make(map[int]int)
	handTrack := make(map[string]int)
	var let string

	for _, letter := range hand {
		let = string(letter)
		handTrack[let] += 1
	}
	for _, count := range handTrack {
		switch count {
		case 5:
			handLookup[5] += 1
		case 4:
			handLookup[4] += 1
		case 3:
			handLookup[3] += 1
		case 2:
			handLookup[2] += 1
		case 1:
			handLookup[1] += 1
		}
	}
	for rank, m := range typeTrack {
		if reflect.DeepEqual(m, handLookup) {
			return rank
		}
	}

	return 10
}

func sortHands(hands []string) []string {
	sort.Slice(hands, func(i, j int) bool {
		return less(hands[i], hands[j])
	})
	return hands
}

func getTypePt2(hand string) int {
	var typeTrack = map[int]map[int]int{0: {5: 1}, 1: {4: 1, 1: 1}, 2: {3: 1, 2: 1}, 3: {3: 1, 1: 2}, 4: {2: 2, 1: 1}, 5: {2: 1, 1: 3}, 6: {1: 5}}
	handLookup := make(map[int]int)
	handTrack := make(map[string]int)
	var let, bestLet string
	var highestNum int

	for _, letter := range hand {
		let = string(letter)
		if let != "J" {
			handTrack[let] += 1
		} else {
			continue
		}
	}
	// fmt.Println(handTrack)
	for _, letter := range hand {
		let = string(letter)
		if let == "J" {
			for key, val := range handTrack {
				if val > highestNum {
					highestNum = val
					bestLet = key
				}
			}
			handTrack[bestLet] += 1
		} else {
			continue
		}
	}
	// fmt.Println(handTrack)

	for _, count := range handTrack {
		switch count {
		case 5:
			handLookup[5] += 1
		case 4:
			handLookup[4] += 1
		case 3:
			handLookup[3] += 1
		case 2:
			handLookup[2] += 1
		case 1:
			handLookup[1] += 1
		}
	}
	for rank, m := range typeTrack {
		if reflect.DeepEqual(m, handLookup) {
			return rank
		}
	}

	return 10
}

func sortHandsPt2(hands []string) []string {
	sort.Slice(hands, func(i, j int) bool {
		return less2(hands[i], hands[j])
	})
	return hands
}
