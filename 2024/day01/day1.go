package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./day1.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var numbers []string
	var list1 []int
	var list2 []int
	var num1 int
	var num2 int

	for scanner.Scan() {
		numbers = strings.Split(scanner.Text(), "   ")
		num1, _ = strconv.Atoi(numbers[0])
		num2, _ = strconv.Atoi(numbers[1])
		list1 = append(list1, num1)
		list2 = append(list2, num2)
	}

	distSum := sumDistances(list1, list2)
	fmt.Println("The Summed Distance is:", distSum)

	similarity := calcSimilarity(list1, countOccurrences(list2))
	fmt.Println("The Similarity Score is:", similarity)
}

func calcSimilarity(list []int, counts map[int]int) int {
	var similarities []int
	// var count int
	var similarity int

	for _, num := range list {
		similarity = num * counts[num]

		similarities = append(similarities, similarity)
	}

	total := sumArray(similarities)

	return total
}

func countOccurrences(list []int) map[int]int {
	count := make(map[int]int)

	for i := range list {
		count[list[i]] = count[list[i]] + 1
	}
	return count
}

func sumDistances(list1 []int, list2 []int) int {
	var distances []int

	sort.Ints(list1)
	sort.Ints(list2)
	for i := range list1 {
		distances = append(distances, absDist(list1[i], list2[i]))
	}
	distSum := sumArray(distances)

	return distSum
}

func absDist(num1 int, num2 int) int {
	dist := num1 - num2
	if dist < 0 {
		dist = -dist
	}
	return dist
}

func sumArray(arr []int) int {
	sum := 0
	for _, valueInt := range arr {
		sum += valueInt
	}
	return sum
}
