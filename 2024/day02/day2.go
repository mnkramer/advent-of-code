package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./day2.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var numbersStr []string
	var nums []int
	var reversedNums []int
	var reportSafe []int
	var trend bool

	for scanner.Scan() {
		numbersStr = strings.Split(scanner.Text(), " ")
		nums = convIntSlice(numbersStr)
		trend = generalTrend(nums)
		nums = problemDampener(nums, trend) // Comment this line to get the part 1 solution (I think)
		reversedNums = make([]int, len(nums))
		copy(reversedNums, nums)
		slices.Reverse(reversedNums)

		if slices.IsSorted(nums) || slices.IsSorted(reversedNums) {
			if checkSafe(nums) {
				reportSafe = append(reportSafe, 1)
			} else {
				reportSafe = append(reportSafe, 0)
			}
		} else {
			reportSafe = append(reportSafe, 0)
		}
	}
	totalSafe := sumArray(reportSafe)
	fmt.Println("Total safe reports is:", totalSafe)
}

func problemDampener(slice []int, dir bool) []int {
	// This is a cursed horrific abomination of if statements, but it solved my problem so?
	var num1 int
	var num2 int
	var num3 int
	var dampened []int

	for i, v := range slice {
		num3 = num2
		num2 = num1
		num1 = v
		if num2 == 0 || num3 == 0 {
			continue
		}
		if num1-num3 >= 1 && num1-num3 <= 3 && dir {
			// if change between a gap of numbers is positive and the list is increasing, check if
			// it is not within that increase, and drop if it does
			if (num2 >= num3 && num2 >= num1) || (num2 <= num3 && num2 <= num1) {
				dampened = append(slice[:i-1], slice[i:]...)
				return dampened
			}
		}
		if num1-num3 <= -1 && num1-num3 >= -3 && !dir {
			// same as above but with decreasing
			if (num2 >= num3 && num2 >= num1) || (num2 <= num3 && num2 <= num1) {
				dampened = append(slice[:i-1], slice[i:]...)
				return dampened
			}
		}
	}

	checkFirst := absValue(slice[0], slice[1])
	if !(checkFirst >= 1 && checkFirst <= 3) {
		// checks if first num is not between 1 and 3 away from the 2nd and drops 1st if so
		dampened = slice[1:]
		return dampened
	}
	if slice[0] > slice[1] && dir {
		// checks to see if first num is decreasing on an increasing line
		dampened = slice[1:]
		return dampened
	}
	if slice[0] < slice[1] && !dir {
		// checks to see if first num is increasing on decreasing line
		dampened = slice[1:]
		return dampened
	}
	checkLast := absValue(slice[len(slice)-1], slice[len(slice)-2])
	if !(checkLast >= 1 && checkLast <= 3) {
		dampened = slice[:len(slice)-1]
		return dampened
	}
	if slice[len(slice)-1] > slice[len(slice)-2] && !dir {
		dampened = slice[:len(slice)-1]
		return dampened
	}
	if slice[len(slice)-1] < slice[len(slice)-2] && dir {
		dampened = slice[:len(slice)-1]
		return dampened
	}
	return slice

}

func checkSafe(slice []int) bool {
	var num1 int
	var num2 int
	var change int

	for _, v := range slice {
		num2 = num1
		num1 = v
		change = absValue(num1, num2)
		if num2 == 0 {
			continue
		}
		if change >= 1 && change <= 3 {
			continue
		} else {
			return false
		}
	}

	return true
}

func absValue(num1 int, num2 int) int {
	val := num1 - num2
	if val < 0 {
		val = -val
	}
	return val
}

func convIntSlice(slice []string) []int {
	newSlice := make([]int, len(slice))
	for i, v := range slice {
		newSlice[i], _ = strconv.Atoi(v)
	}
	return newSlice
}

func sumArray(arr []int) int {
	sum := 0
	for _, valueInt := range arr {
		sum += valueInt
	}
	return sum
}

func generalTrend(slice []int) bool {
	var num1 int
	var num2 int
	var change int
	var totalChange int

	for _, v := range slice {
		num2 = num1
		num1 = v
		if num2 != 0 {
			change = num1 - num2
			if change >= 0 {
				totalChange += 1
			}
			if change < 0 {
				totalChange -= 1
			}
		}
	}
	if totalChange >= 0 {
		return true //increasing line
	} else {
		return false //decreasing line
	}

}
