package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./day3.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var parsed []string
	var result []int
	var sum int
	var allResults []int

	var parsed2 []string
	var result2 []int
	var sum2 int
	var allResults2 []int
	var lastInstruction string
	for scanner.Scan() {
		parsed = parseStrP1(scanner.Text())
		result = doInstructionsP1(parsed)
		sum = sumArray(result)
		allResults = append(allResults, sum)

		parsed2 = parseStrP2(scanner.Text())
		result2, lastInstruction = doInstructionsP2(parsed2, lastInstruction)
		sum2 = sumArray(result2)
		allResults2 = append(allResults2, sum2)
	}

	totalSum := sumArray(allResults)
	fmt.Println("The total sum of instructions is:", totalSum)

	totalSum2 := sumArray(allResults2)
	fmt.Println("The total sum of corrupted instructions is:", totalSum2)

}

func doInstructionsP2(slice []string, str string) ([]int, string) {
	var split string
	var nums []string
	var num1 int
	var num2 int
	var result int
	var allResults []int
	var include string

	if str == "" {
		include = "do()"
	} else {
		include = str
	}

	for _, v := range slice {
		if v == "do()" || v == "don't()" {
			include = v
			continue
		}
		if include == "do()" {
			split = strings.TrimRight(strings.Split(v, "(")[1], ")")
			nums = strings.Split(split, ",")
			num1, _ = strconv.Atoi(nums[0])
			num2, _ = strconv.Atoi(nums[1])
			result = num1 * num2
			allResults = append(allResults, result)
		}
		if include == "don't()" {
			continue
		}

	}
	lastInclude := include

	return allResults, lastInclude
}

func parseStrP2(str string) []string {
	var allResult []string

	reg := regexp.MustCompile(`mul\(\d+,\d+\)|do\(\)|don't\(\)`)

	result := reg.FindAll([]byte(str), -1)
	for _, v := range result {
		allResult = append(allResult, string(v))
	}

	return allResult
}

func doInstructionsP1(slice []string) []int {
	var split string
	var nums []string
	var num1 int
	var num2 int
	var result int
	var allResults []int

	for _, v := range slice {
		split = strings.TrimRight(strings.Split(v, "(")[1], ")")
		nums = strings.Split(split, ",")
		num1, _ = strconv.Atoi(nums[0])
		num2, _ = strconv.Atoi(nums[1])
		result = num1 * num2
		allResults = append(allResults, result)
	}
	return allResults
}

func parseStrP1(str string) []string {
	var allResult []string

	reg := regexp.MustCompile(`mul\(\d+,\d+\)`)

	result := reg.FindAll([]byte(str), -1)
	for _, v := range result {
		allResult = append(allResult, string(v))
	}

	return allResult
}

func sumArray(arr []int) int {
	sum := 0
	for _, valueInt := range arr {
		sum += valueInt
	}
	return sum
}
