package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	file, err := os.Open("./day4.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var area [][]string
	var line []string

	for scanner.Scan() {
		line = strings.Split(scanner.Text(), "")
		area = append(area, line)
	}
	// fmt.Println(area)
	totalCount := wordSearchXMAS(area)
	fmt.Println("Total XMAS Count is:", totalCount)

	totalCount = wordSearchMAS(area)
	fmt.Println("Total X-MAS Count is:", totalCount)
}

func wordSearchMAS(cloud [][]string) int {
	var count int
	var word string

	nwse := false
	nesw := false
	maxX := len(cloud[0]) - 1
	maxY := len(cloud) - 1

	for y, v := range cloud {
		for x, w := range v {
			if w == "A" {
				if (x-1 >= 0 && y-1 >= 0) && (x+1 <= maxX && y+1 <= maxY) { //NW-SE
					word = cloud[y-1][x-1] + cloud[y][x] + cloud[y+1][x+1]
					if word == "MAS" || word == "SAM" {
						nwse = true
					}
				}
				if (x-1 >= 0 && y+1 <= maxY) && (x+1 <= maxX && y-1 >= 0) { //NE-SW
					word = cloud[y+1][x-1] + cloud[y][x] + cloud[y-1][x+1]
					if word == "MAS" || word == "SAM" {
						nesw = true
					}
				}
				if nwse && nesw {
					count += 1
				}
				nwse = false
				nesw = false
			}
		}
	}

	return count
}

func wordSearchXMAS(cloud [][]string) int {
	var count int
	var word string

	maxX := len(cloud[0]) - 1
	maxY := len(cloud) - 1

	for y, v := range cloud {
		for x, w := range v {
			if w == "X" {
				if x-3 >= 0 { //WEST
					word = cloud[y][x] + cloud[y][x-1] + cloud[y][x-2] + cloud[y][x-3]
					if word == "XMAS" {
						count += 1
					}
				}
				if x-3 >= 0 && y-3 >= 0 { //NORTHWEST
					word = cloud[y][x] + cloud[y-1][x-1] + cloud[y-2][x-2] + cloud[y-3][x-3]
					if word == "XMAS" {
						count += 1
					}
				}
				if y-3 >= 0 { //NORTH
					word = cloud[y][x] + cloud[y-1][x] + cloud[y-2][x] + cloud[y-3][x]
					if word == "XMAS" {
						count += 1
					}
				}
				if x+3 <= maxX && y-3 >= 0 { //NORTHEAST
					word = cloud[y][x] + cloud[y-1][x+1] + cloud[y-2][x+2] + cloud[y-3][x+3]
					if word == "XMAS" {
						count += 1
					}
				}
				if x+3 <= maxX { //EAST
					word = cloud[y][x] + cloud[y][x+1] + cloud[y][x+2] + cloud[y][x+3]
					if word == "XMAS" {
						count += 1
					}
				}
				if x+3 <= maxX && y+3 <= maxY { //SOUTHEAST
					word = cloud[y][x] + cloud[y+1][x+1] + cloud[y+2][x+2] + cloud[y+3][x+3]
					if word == "XMAS" {
						count += 1
					}
				}
				if y+3 <= maxY { //SOUTH
					word = cloud[y][x] + cloud[y+1][x] + cloud[y+2][x] + cloud[y+3][x]
					if word == "XMAS" {
						count += 1
					}
				}
				if x-3 >= 0 && y+3 <= maxY { //SOUTHWEST
					word = cloud[y][x] + cloud[y+1][x-1] + cloud[y+2][x-2] + cloud[y+3][x-3]
					if word == "XMAS" {
						count += 1
					}
				}
			}
		}
	}

	return count
}
