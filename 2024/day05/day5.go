package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./day5.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	rules := make(map[int][]int)
	var allMids []int
	var allMidsFailed []int

	for scanner.Scan() {
		text := scanner.Text()
		if len(text) == 5 {
			nums := strings.Split(text, "|")
			num1, _ := strconv.Atoi(nums[0])
			num2, _ := strconv.Atoi(nums[1])
			rules[num1] = append(rules[num1], num2)
		} else {
			update := strings.Split(text, ",")
			isValid := checkValid(rules, update)
			if isValid {
				midVal := findMid(update)
				allMids = append(allMids, midVal)
			} else {
				update = fixUpdate(rules, update)
				if update == nil {
					log.Fatal("UPDATE BROKEY")
				}
				midVal := findMid(update)
				allMidsFailed = append(allMidsFailed, midVal)
			}
		}
	}
	// fmt.Println(allMids)
	sumMids := sumArray(allMids)
	fmt.Println("The sum of all the valid updates is:", sumMids)

	sumMidsFailed := sumArray(allMidsFailed)
	fmt.Println("The sum of all the fixed updates is:", sumMidsFailed)
}

func fixUpdate(rules map[int][]int, update []string) []string {
	var newUpdate []string

	for i, val := range update {
		afterNums := update[i+1:]
		valInt, _ := strconv.Atoi(val)
		for _, num := range afterNums {
			numInt, _ := strconv.Atoi(num)
			if slices.Contains(rules[valInt], numInt) {
				continue
			} else {
				if !slices.Contains(newUpdate, num) {
					newUpdate = append(newUpdate, num)
				}
			}
		}
		if !slices.Contains(newUpdate, val) {
			newUpdate = append(newUpdate, val)
		}

	}
	if checkValid(rules, newUpdate) {
		return newUpdate
	} else {
		newUpdate = fixUpdate(rules, newUpdate)
	}
	return newUpdate
}

func checkValid(rules map[int][]int, update []string) bool {
	// fmt.Println(rules)
	for i, val := range update {
		afterNums := update[i+1:]
		valInt, _ := strconv.Atoi(val)
		for _, num := range afterNums {
			numInt, _ := strconv.Atoi(num)
			if slices.Contains(rules[valInt], numInt) {
				continue
			} else {
				return false
			}
		}
	}
	return true
}

func findMid(slice []string) int {
	middle := len(slice) / 2
	mid := slice[middle]
	midVal, _ := strconv.Atoi(mid)
	return midVal
}

func sumArray(arr []int) int {
	sum := 0
	for _, valueInt := range arr {
		sum += valueInt
	}
	return sum
}
