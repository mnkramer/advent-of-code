package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
)

func main() {
	file, err := os.Open("./day6.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var guardMap [][]string
	var loopCount int

	for scanner.Scan() {
		line := strings.Split(scanner.Text(), "")
		guardMap = append(guardMap, line)
	}
	startPos := findStart(guardMap)
	guardMapMoved := moveGuard(guardMap, startPos)
	// fmt.Println(guardMap)
	count := countPos(guardMapMoved)
	fmt.Println("The distinct positions visited is:", count)

	for y, v := range guardMap {
		for x := range v {
			testPos := []int{y, x}
			posTest := testObstacle(guardMap, startPos, testPos)
			if posTest {
				loopCount += 1
			}
		}
	}
	fmt.Println("The count of obstacles to make a loop is:", loopCount)
}

func testObstacle(guard [][]string, startPos []int, testPos []int) bool {
	// Y, X

	guardMap := deepCopy(guard)
	dir := 0
	directions := [][]int{{-1, 0}, {0, 1}, {1, 0}, {0, -1}}
	guardInMap := true
	currentPos := startPos
	if !reflect.DeepEqual(testPos, currentPos) {
		guardMap[testPos[0]][testPos[1]] = "O"
	} else {
		return false
	}

	inARow := 0
	lastObstacle := "#"

	for guardInMap {
		guardMap[currentPos[0]][currentPos[1]] = "X"
		nextPos := []int{currentPos[0] + directions[dir][0], currentPos[1] + directions[dir][1]}
		if (nextPos[0] < 0 || nextPos[0] >= len(guardMap)) || (nextPos[1] < 0 || nextPos[1] >= len(guardMap[0])) {
			break
		}
		if guardMap[nextPos[0]][nextPos[1]] == "#" || guardMap[nextPos[0]][nextPos[1]] == "O" {

			dir += 1
			if dir >= 4 {
				dir = 0
			}
			if guardMap[nextPos[0]][nextPos[1]] == "O" && lastObstacle == "O" {
				inARow += 1
			} else {
				inARow = 0
			}
			lastObstacle = guardMap[nextPos[0]][nextPos[1]]
			guardMap[nextPos[0]][nextPos[1]] = "O"

			if inARow >= 500 {
				// printMap(guardMap)
				return true

			}
		} else {
			currentPos = []int{currentPos[0] + directions[dir][0], currentPos[1] + directions[dir][1]}
		}

	}
	return false
}

func countPos(guardMap [][]string) int {
	var count int
	for _, v := range guardMap {
		for _, b := range v {
			if b == "X" {
				count += 1
			}
		}
	}
	return count
}

func moveGuard(guard [][]string, startPos []int) [][]string {
	// Y, X
	guardMap := deepCopy(guard)
	dir := 0
	directions := [][]int{{-1, 0}, {0, 1}, {1, 0}, {0, -1}}
	guardInMap := true
	currentPos := startPos

	for guardInMap {
		guardMap[currentPos[0]][currentPos[1]] = "X"
		nextPos := []int{currentPos[0] + directions[dir][0], currentPos[1] + directions[dir][1]}
		if (nextPos[0] < 0 || nextPos[0] >= len(guardMap)) || (nextPos[1] < 0 || nextPos[1] >= len(guardMap[0])) {
			break
		}
		// fmt.Println(nextPos)
		if guardMap[nextPos[0]][nextPos[1]] == "#" {
			dir += 1
			if dir >= 4 {
				dir = 0
			}
		}

		currentPos = []int{currentPos[0] + directions[dir][0], currentPos[1] + directions[dir][1]}
	}
	return guardMap
}

func findStart(guardMap [][]string) []int {
	var startPos []int
	for y, v := range guardMap {
		for x, b := range v {
			if b == "^" {
				startPos = []int{y, x}
			}
		}
	}
	return startPos
}

// func printMap(guardMap [][]string) {
// 	for _, v := range guardMap {
// 		for _, m := range v {
// 			fmt.Print(m)
// 		}
// 		fmt.Print("\n")
// 	}
// }

func deepCopy(guardMap [][]string) [][]string {
	duplicate := make([][]string, len(guardMap))
	for i := range guardMap {
		duplicate[i] = make([]string, len(guardMap[i]))
		copy(duplicate[i], guardMap[i])
	}
	return duplicate
}
